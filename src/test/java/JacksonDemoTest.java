import com.fasterxml.jackson.core.JsonProcessingException;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class JacksonDemoTest {

    @Test
    public void testHouseSerializeDeserialize() throws PersonException, JsonProcessingException {
        List<Flat> flats = new LinkedList<>();

        flats.add(new Flat(1, 110, Collections.singletonList(new Person(
                "Иван", "Орлов", "Алексеевич", 1, 5, 1999))));
        flats.add(new Flat(2, 55, Collections.singletonList(new Person(
                "Александр", "Давыдов", "Сергеевич", 4, 1, 1977))));

        List<Person> personFlat3 = new LinkedList<>();
        personFlat3.add(new Person("Олег", "Власов", "Юрьевич", 13, 2, 2002));
        personFlat3.add(new Person("Сергей", "Власов", "Денисович", 17, 8, 1980));

        List<Person> personFlat4 = new LinkedList<>();
        personFlat4.add(new Person("Евгений", "Жданов", "Павловна", 25, 3, 1990));
        personFlat4.add(new Person("София", "Жданова", "Петровна", 19, 1, 2003));
        personFlat4.add(new Person("Елизавета", "Жданова", "Павловна", 7, 9, 1981));

        flats.add(new Flat(5, 23, personFlat3));
        flats.add(new Flat(11, 80, personFlat4));

        House house = new House("23", "Ylica Severnya 7",
                new Person("Елизавета", "Жданова", "Павловна", 1, 2,1990), flats);

        String jsonHouse = JacksonDemo.writeJsonString(house);
        House jsonToHouse = JacksonDemo.readJsonString(jsonHouse);

        assertEquals(house, jsonToHouse);

    }

    @Test
    public void testJsonEqualWithSameHouse() throws IOException, PersonException {
        List<Flat> flats = new LinkedList<>();
        flats.add(new Flat(1, 110, Collections.singletonList(new Person(
                "Иван", "Орлов", "Алексеевич", 1, 5, 1999))));
        flats.add(new Flat(2, 55, Collections.singletonList(new Person(
                "Александр", "Давыдов", "Сергеевич", 4, 1, 1977))));

        House house1 = new House("23", "Улица Северная 7",
                new Person("Елизавета", "Жданова", "Павловна", 1, 2,1990), flats);
        House house2 = new House("23", "Улица Северная 7",
                new Person("Елизавета", "Жданова", "Павловна", 1, 2,1990), flats);

        String json1 = JacksonDemo.writeJsonString(house1);
        String json2 = JacksonDemo.writeJsonString(house2);
        assertTrue(JacksonDemo.jsonEqual(json1, json2));
    }

    @Test
    public void testJsonEqualWithDifferentHouse() throws IOException, PersonException {
        List<Flat> flats1 = new LinkedList<>();
        flats1.add(new Flat(1, 110, Collections.singletonList(new Person(
                "Иван", "Орлов", "Алексеевич", 1, 5, 1999))));
        List<Flat> flats2 = new LinkedList<>();
        flats2.add(new Flat(1, 110, Collections.singletonList(new Person(
                "Иван", "Фомин", "Алексеевич", 1, 5, 1999))));
        List<Flat> flats3 = new LinkedList<>();
        flats3.add(new Flat(2, 55, Collections.singletonList(new Person(
                "Александр", "Давыдов", "Сергеевич", 4, 1, 1977))));

        House house1 = new House("23", "Улица Северная 7",
                new Person("Елизавета", "Жданова", "Павловна", 1, 2,1990), flats1);
        House house2 = new House("23", "Улица Северная 7",
                new Person("Елизавета", "Жданова", "Павловна", 1, 2,1990), flats2);
        House house3 = new House("23", "Улица Северная 7",
                new Person("Елизавета", "Жданова", "Павловна", 1, 2,1990), flats3);

        String json1 = JacksonDemo.writeJsonString(house1);
        String json2 = JacksonDemo.writeJsonString(house2);
        String json3 = JacksonDemo.writeJsonString(house3);
        assertFalse(JacksonDemo.jsonEqual(json1, json2));
        assertFalse(JacksonDemo.jsonEqual(json1, json3));


    }

}
