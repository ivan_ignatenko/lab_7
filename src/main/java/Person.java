import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.io.ObjectOutput;
import java.io.Serializable;
import java.util.Objects;

/**
 * Класс Person (человек), содержит поля:
 * Ф.И.О. (раздельно), дату рождения
 */
public class Person implements Serializable {
    private String name, lastName, surName;
    private int day, month, year;

    Person(){
        name = "";
        lastName = "";
        surName = "";
        day = 0;
        month = 0;
        year = 0;
    }

    public Person(String name, String lastName, String surName, int day, int month, int year) throws PersonException {
        if(name == null || "".equals(name) || lastName == null || "".equals(lastName) || surName == null
                || "".equals(surName)){
            throw new PersonException("Empty name or last name or surname");
        }
        else if(day <= 0 || day > 31){
            throw new PersonException("Incorrect day");
        }
        else if(month <= 0 || month > 12){
            throw new PersonException("Incorrect month");
        }
        else if(year < 0){
            throw new PersonException("Incorrect year");
        }
        this.name = name;
        this.lastName = lastName;
        this.surName = surName;
        this.day = day;
        this.month = month;
        this.year = year;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) throws PersonException {
        if(name == null || "".equals(name)){
            throw new PersonException("Empty name");
        }
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) throws PersonException {
        if(lastName == null || "".equals(lastName)){
            throw new PersonException("Empty last name");
        }
        this.lastName = lastName;
    }

    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) throws PersonException {
        if(surName == null || "".equals(surName)){
            throw new PersonException("Empty surname");
        }
        this.surName = surName;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) throws PersonException {
        if(day <= 0 || day > 31){
            throw new PersonException("Incorrect day");
        }
        this.day = day;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) throws PersonException {
        if(month <= 0 || month > 12){
            throw new PersonException("Incorrect month");
        }
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) throws PersonException {
        if(year < 0){
            throw new PersonException("Incorrect year");
        }
        this.year = year;
    }

    @Override
    public String toString() {
        return "Person{" + "name='" + name + '\'' + ", lastName='" + lastName
                + '\'' + ", surName='" + surName + '\'' + ", day=" + day
                + ", month=" + month + ", year=" + year + '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Person person = (Person) o;
        return day == person.day && month == person.month && year == person.year
                && Objects.equals(name, person.name) && Objects.equals(lastName, person.lastName) &&
                Objects.equals(surName, person.surName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, lastName, surName, day, month, year);
    }

    public void serialize(@NotNull ObjectOutput out) throws IOException {
        out.writeObject(this);
    }

}
