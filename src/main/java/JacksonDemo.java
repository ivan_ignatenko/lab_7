import com.fasterxml.jackson.core.*;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;


public class JacksonDemo {
    /**
     * 8.Метод сериализации объекта типа House в строку(использующий data binding)
     */
    public static String writeJsonString(House house) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(house);
    }

    /**
     * 8.Метод десериализации строки в объект типа House(использующий data binding)
     */
    public static House readJsonString(String json) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(json, House.class);
    }

    /**
     * 9. Метод, сравнивает две json-строки на равенство.
     */
    public static boolean jsonEqual(String json1, String json2) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode js1 = mapper.readTree(json1);
        JsonNode js2 = mapper.readTree(json2);
        return js1.equals(js2);
    }

}
