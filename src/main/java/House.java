import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.io.ObjectOutput;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * класс House (дом), содержит поля:
 * кадастровый номер дома (строка), адрес, старшего по дому(человек), список квартир.
 */
public class House implements Serializable {
    private String cadastralNumber, address;
    private Person houseWife;
    private List<Flat> apartments;

    House() throws PersonException {
        cadastralNumber = "";
        address = "";
        houseWife = new Person();
        apartments = new ArrayList<>();
    }

    public House(String cadastralNumber, String address, Person houseWife, List<Flat> apartments) {
        this.cadastralNumber = cadastralNumber;
        this.address = address;
        this.houseWife = houseWife;
        this.apartments = apartments;
    }

    public String getCadastralNumber() {
        return cadastralNumber;
    }

    public void setCadastralNumber(String cadastralNumber) {
        this.cadastralNumber = cadastralNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Person getHouseWife() {
        return houseWife;
    }

    public void setHouseWife(Person houseWife) {
        this.houseWife = houseWife;
    }

    public List<Flat> getApartments() {
        return apartments;
    }

    public void setApartments(List<Flat> apartments) {
        this.apartments = apartments;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        House house = (House) o;
        return Objects.equals(cadastralNumber, house.cadastralNumber) && Objects.equals(address, house.address) && Objects.equals(houseWife, house.houseWife) && Objects.equals(apartments, house.apartments);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cadastralNumber, address, houseWife, apartments);
    }

    @Override
    public String toString() {
        return "House{" + "cadastralNumber='" + cadastralNumber
                + '\'' + ", address='" + address + '\'' + ", houseWife="
                + houseWife + ", apartments=" + apartments + '}';
    }

    public void serialize(@NotNull ObjectOutput out) throws IOException {
        out.writeObject(this);
    }

}
